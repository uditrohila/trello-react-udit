import React from 'react';
import { BrowserRouter, Switch , Route , Redirect } from 'react-router-dom/cjs/react-router-dom.min';
import './App.css';
import Boards from './components/Boards';
import EachBoardPage from './components/EachBoardPage';
import NavBar from './components/NavBar';
import PageNotFound from './components/PageNotFound';

function App() {
  return (
      <BrowserRouter>
       <React.Fragment>
       <NavBar/>
       <Switch>
       <Route exact path="/">
              <Redirect to="/boards" />
            </Route>
          <Route exact path="/boards" component={Boards}/>
          <Route exact path="/boards/:boardId" component={EachBoardPage}/>
          <Route  component={PageNotFound}/>
       </Switch>
       </React.Fragment>
      </BrowserRouter>
     
  );
}

export default App;
