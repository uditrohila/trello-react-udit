import React, { Component } from 'react'
import { FaTasks } from "react-icons/fa";
import AllCheckItemsinCheckList from './AllCheckItemsInCheckList';
class EachCheckList extends Component {
    state = {  } 
    render() { 
        console.log(this.props.CardDetails)
        console.log(this.props.checkList,"each checklist container");
        return (
            <div className="checklist-background">
            <div class="all-checklist-wrapper" >
                 <div className="all-checklist d-flex">
                  <div className="Fatsks-icon"><FaTasks/></div>   
                <div className="checklist-name">{this.props.checkList.name}</div>
                <button className="deletebtn-checklist" onClick={() => this.props.onhandleDeleteEachCheckList(this.props.checkList.id)}>delete</button>
              </div>
              <AllCheckItemsinCheckList 
              checkList = {this.props.checkList}
              CardDetails = {this.props.CardDetails}
              EachCardDetails = {this.props.EachCardDetails} />
            </div>
            </div>
        );
    }
}
 
export default EachCheckList;