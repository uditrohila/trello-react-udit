import React, { Component } from 'react'
import { getAllCardsOfList } from './fetchapi';
import { Button } from 'react-bootstrap';
import { createNewCardInList } from './fetchapi';
import { DeleteCardInList } from './fetchapi';
import EachCardComponent from './EachCardComponent';
class Cards extends Component {
    state = { 
        Cards: [],
        addCardToggle: true,
        inputTitle: "",
        checklistModal: false,

     } 


     componentDidMount = () => {
           getAllCardsOfList(this.props.listDetail.id)
           .then((res) => {this.setState({Cards: res.data})})
     }

     handleInput = (e) => {
         this.setState({inputTitle: e.target.value})
     }

     handleSumbit = (listid) => {
        //  console.log(listid, "each list id", "submit");
        //  console.log(this.state.inputTitle, "sumbit")
         createNewCardInList(this.state.inputTitle,listid)
         .then((res) => this.setState({Cards: [...this.state.Cards, res.data]}))
         .then((res) => this.setState({addCardToggle: !this.state.addCardToggle}))
         .then((res) => this.setState({inputTitle: ""}))
     }

     handleAddCardToggle = () => {
         this.setState({addCardToggle: !this.state.addCardToggle})
     }

     handleClose = () => {
         this.setState({addCardToggle: !this.state.addCardToggle})
     }

     handleDeleteCard = (id) => {
         console.log(id);
         DeleteCardInList(id)
         .then((res) => this.setState({Cards : this.state.Cards.filter((card) => card.id !== id)}))
     }

  
     handleChecklistState = () => {
         this.setState({checklistState: !this.state.checklistState})
     }

     
    render() { 
        console.log(this.props.listDetail.id, this.props.listDetail.name);
        console.log(this.state.Cards, "cards")
        // console.log(this.state.Cards, "cards");
        return (
            <div className="card-wrapper">
            <div className="card-container">
             {this.state.Cards.map((card) => {
                 return (
                       <div>
                      {<EachCardComponent 
                      CardDetails={card}
                      onHandleDeleteCard = {this.handleDeleteCard}
                      listDetail = {this.props.listDetail} />}
                      </div>
                       
                 )
             })}
              {/* <div className="card">walk</div>
              <div className="card">go to gym</div>
              <div className="card">go for shopping</div>
              <div className="card">go for shopping</div>
              <div className="card">go for shopping</div> */}

            </div>
           
            <div className='add-btn-hover'>
            {/* <button className="add-btn">+ Add a card</button> */}
            {this.state.addCardToggle?(
               <button onClick={this.handleAddCardToggle} className="add-btn">+ Add a card</button> 
            ):(
                <div className="card-create">
                <input className="list-create-input" type="text" onChange={this.handleInput} value={this.state.inputTitle} placeholder="Enter list title"></input>
                <div className="list-create-add-list d-flex"><Button onClick = {() => this.handleSumbit(this.props.listDetail.id)} className="list-create-btn">Add list</Button> <span onClick={this.handleClose} className="remove-addlist">x</span></div>
               
            </div>
            )}
            </div>
          </div>
        );
    }
}
 
export default Cards;