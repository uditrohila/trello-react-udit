import axios from "axios";


export function createBoard(title) {
    return axios.post(`https://api.trello.com/1/boards/?name=${title}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)  
}


export function createList(listname, id){
    return axios.post(`https://api.trello.com/1/lists?name=${listname}&idBoard=${id}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function deleteList(id) {
    return axios.put(`https://api.trello.com/1/lists/${id}/closed?value=true&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function getAllCardsOfList(id) {
    return axios.get(`https://api.trello.com/1/lists/${id}/cards?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function createNewCardInList(title, listid)
{
    return axios.post(`https://api.trello.com/1/cards?name=${title}&idList=${listid}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function DeleteCardInList(cardId){
    return axios.delete(`https://api.trello.com/1/cards/${cardId}?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function GetAllCheckListsOfCard(cardId){
    return axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}


export function CreateCheckListInCard(cardId, name){
    return axios.post(`https://api.trello.com/1/checklists?name=${name}&idCard=${cardId}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function DeleteCheckListInCard(checkListId){
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function GetAllCheckItemsOfCheckList(checkListId){
    return axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function CreateACheckIteminCheckList(checkListId, name){
    return axios.post(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${name}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function DeleteACheckItemFromCheckList(checkListId, checkItemId){
    return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}

export function UpdateCheckItemsOnCard(status, cardId, checkItemId){
    return axios.put(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${status}&key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
}
