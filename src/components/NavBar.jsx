import React, { Component } from 'react';
import { BsFillGrid3X3GapFill } from "react-icons/bs";
import { FaTrello } from "react-icons/fa";
import { Link } from 'react-router-dom/cjs/react-router-dom.min';
class NavBar extends Component {
    state = {  } 
    render() { 
        return (
            <div>
                <div className="navbar-menu">
                    <Link to="/boards"><BsFillGrid3X3GapFill class="grid-icon"/></Link>
                   <FaTrello className="home-icon"/> 
                   <h4 className="nav-title">Trello</h4>
                </div>
            </div>
        );
    }
}
 
export default NavBar;
{/* <h4 className="nav-home">Home</h4> */}