import React, { Component } from "react";
import { FaTrash } from "react-icons/fa";
import Modal from "react-modal/lib/components/Modal";
import CheckListContainer from "./CheckListContainer";
class EachCardComponent extends Component {
  state = {
    handleChecklistState: false,
    modal: false,
  };

  handleChecklistState = (cardId) => {
    console.log(cardId, "cardid");
    this.setState({modal: !this.state.modal})
    
  };

  handleToggleModal = () => {
      this.setState({modal: !this.state.modal})
  }

  render() {
    return (
      <div>
        <div className="card-content d-flex" >
          <div
            
            className="card"
          >
             <div className="card-details-container  d-flex ">
            <div  className="card-details" onClick={() => this.handleChecklistState(this.props.CardDetails.id)}>{this.props.CardDetails.name}</div>
            <span
            className="card-remove-btn d-flex"
            onClick={() => {
              this.props.onHandleDeleteCard(this.props.CardDetails.id);
            }}
          >
            <FaTrash/>
          </span>  
             </div> 
            
            
          </div>
        
        </div>

        {/*modal container of each card click  */}
        <div className="modal-height">
          <Modal
            isOpen={this.state.modal}
            style={{
              overlay: {
                backgroundColor: "transparent",
              },

              content: {
                // color: 'orange',
                width: "50%",
                alignItems: "center",
                height: "80%",
                margin: "auto",
              },
            }}
          >
            <CheckListContainer CardDetails = {this.props.CardDetails}
            handleToggleModal = {this.handleToggleModal}
            listDetail = {this.props.listDetail}
            EachCardDetails = {this.props.CardDetails}/>
          </Modal>
        </div>
      </div>
    );
  }
}

export default EachCardComponent;
