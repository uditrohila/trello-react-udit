import axios from "axios";
import React, { Component } from "react";
import Board from "./Board";
import { createBoard } from "./fetchapi";
import Loading from "./Loading";
import ModalContainer from "./ModalContainer";
class Boards extends Component {
  state = {
    boards: [
    ],
    modalShow: false,
    loading: false,
  };

  componentDidMount = () => {
    this.setState({loading: !this.state.loading})
    axios
      .get(
        " https://api.trello.com/1/members/me/boards?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d"
      )
      .then((res) => this.setState({ boards: res.data,
      loading: !this.state.loading }));
    //   .then((res) => console.log(res.data));
  };
  
  // updating on ui
  handleAddNewBoard = (title) => {
     createBoard(title)
     .then((res) => res.data)
     .then((newBoard) => {
       console.log(newBoard);
       this.setState({boards: [...this.state.boards, newBoard]})
     })
    // console.log(title);

  }

  handleModal = () => {
    this.setState({ modalShow: !this.state.modalShow });
    console.log("true");
    
  };
  
  render() {
    // console.log(this.state.boards);
    return (
      <React.Fragment>
       <h5 className="boards-heading"> Your Boards</h5> 
       {this.state.loading&&<Loading/>}
      <div className="d-flex justify-content-center flex-wrap board-containers m-auto ">
        {this.state.boards.map((board) => {
          return (
            <Board
              key={board.id}
              id={board.id}
              name={board.name}
            />
          );
        })}
        <div onClick={this.handleModal} className="  new-board">
          <div className="boards-status">
          <h5 className="text-center">create new board</h5>
          <h6 className="text-center">boards remaining: {10-this.state.boards.length}</h6>
          </div>
          
        </div>
        <ModalContainer modalShow={this.state.modalShow}
        onHandleModal = {this.handleModal}
        onHandleAddNewBoard = {this.handleAddNewBoard}/>
      </div>
      </React.Fragment>
    );
  }
}

export default Boards;
