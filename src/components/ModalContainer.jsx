import React, { Component } from "react";
// import createBoard from "./fetchapi";
import { createBoard } from "./fetchapi";
import { Button , Modal} from 'react-bootstrap';

class ModalContainer extends Component {
  state = {
    //   modal: this.props.modal,
    title: "",
  };

  handleChange = (e) => {
    this.setState({ title: e.target.value });
  };

  handleSubmit = (e) => {
    // createBoard(this.state.title);
    this.props.onHandleAddNewBoard(this.state.title);
    this.props.onHandleModal();
  };
  render() {
    // console.log(this.props.popupstate, "hello");
    //   console.log(this.state.title);
    // console.log(this.state.title);

    return (
      <>

        <Modal show={this.props.modalShow} onHide={this.props.onHandleModal}>
          <Modal.Header closeButton>Board Title</Modal.Header>

          <Modal.Body>
            <input
              className="modal-input"
              onChange={this.handleChange}
              value={this.state.title}
              type="text"
              placeholder="Add board tilte"
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick ={this.handleSubmit}>Create Board</Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ModalContainer;
