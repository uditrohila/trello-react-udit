import React, { Component } from "react";
import { FaCreditCard } from "react-icons/fa";
import { ImCross } from "react-icons/im";
import EachCheckList from "./EachCheckList";
import { CreateCheckListInCard, DeleteCheckListInCard, GetAllCheckListsOfCard } from "./fetchapi";
class CheckListContainer extends Component {
  state = {
    AllCheckListInCard: [],
    inputText: "",
    addListItemToggle: true

  }

  componentDidMount = () => {
    GetAllCheckListsOfCard(this.props.CardDetails.id)
      .then((res) => this.setState({AllCheckListInCard: res.data}))
  }

  handleChange = (e) => {
      this.setState({inputText: e.target.value})
  }

  handleSubmit = (e) => {
    //   api call to create the check list
    CreateCheckListInCard(this.props.CardDetails.id, this.state.inputText)
    .then((res) => this.setState({AllCheckListInCard: [...this.state.AllCheckListInCard, res.data]}))
    .then((res) => this.setState({addListItemToggle: !this.state.addListItemToggle}))
    .then((res) => this.setState({inputText: ""}))
    // console.log(this.state.inputText, "data come to add listitem");
    // this.setState({addListItemToggle: !this.state.addListItemToggle})

  }

  handleChangeState = () => {
      this.setState({addListItemToggle: !this.state.addListItemToggle})
      
  }

  handleClose = () => {
      this.setState({addListItemToggle: !this.state.addListItemToggle})
  }

  handleDeleteEachCheckList = (checkListId) => {
    //   console.log(checkListId, "checklist id for deletion");
    //   console.log("checklist deleted");
    DeleteCheckListInCard(checkListId)
    .then((res) => this.setState({AllCheckListInCard: this.state.AllCheckListInCard.filter((checklist) => checklist.id !== checkListId)}))
  }

  
  render() {
      console.log(this.props.listDetail, "checklist container")
      console.log(this.props.CardDetails, "from checklist container");
    return (
      <div>
        <div className="checklist-container">
            <div className="checklist-header d-flex">
         <div className="fac-credit-card">
         <FaCreditCard/></div>       
        
        <div className="card-title-container">
            <h4 className="card-checklist-title">{this.props.CardDetails.name}</h4>
            <h5 className="checklist-container-list-name">In List {this.props.listDetail.name}</h5>
          </div>
              
          <div className="checklist-remove">
          <span  onClick={this.props.handleToggleModal}><ImCross/></span>
          </div>

                 
            </div>
    
         
          {/* map for display all check list */}
          <div className="add-checklist">
                     {this.state.addListItemToggle?(
                         <button className="btn-primary" onClick={this.handleChangeState}>Add checkList</button>
                     ):(
                        <div>
                        <input type="text" onChange={this.handleChange} value={this.state.inputText} ></input>
                        <button className="btn-primary m-2" onClick={this.handleSubmit}>Add</button>
                        <button className="close-btn" onClick={this.handleClose}>Close</button>
                    </div>
                     )}
                </div>
          {this.state.AllCheckListInCard.map((checklist) => {
              console.log(checklist.id, "each checklist id");
            return (
              <EachCheckList checkList = {checklist}
              onhandleDeleteEachCheckList = {this.handleDeleteEachCheckList}
              CardDetails = {this.props.CardDetails}
              EachCardDetails = {this.props.EachCardDetails}/>
            );
          })}
        </div>
      </div>
    );
  }
}

export default CheckListContainer;
