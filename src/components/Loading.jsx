import React, { Component } from 'react'
class Loading extends Component {
    render() { 
        return (
            <div className="text-center">
            <img src="/images/loading.gif" alt="loading-gif"/>
            </div>
        );
    }
}
 
export default Loading;