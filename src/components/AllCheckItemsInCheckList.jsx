import { FaTrash } from "react-icons/fa";
import React, { Component } from "react";
import { CreateACheckIteminCheckList, DeleteACheckItemFromCheckList, GetAllCheckItemsOfCheckList, UpdateCheckItemsOnCard } from "./fetchapi";
class AllCheckItemsinCheckList extends Component {
  state = {
      AllCheckItems: [],
      addCheckItem: true,
      inputItem: "",
  };

  componentDidMount = () => {
      GetAllCheckItemsOfCheckList(this.props.checkList.id)
      .then((res) => this.setState({AllCheckItems: res.data}))
  }

  handleStateToggle = () => {
      this.setState({addCheckItem: !this.state.addCheckItem})
  }

  handleChange = (e) => {
      this.setState({inputItem: e.target.value})
  }

  handleSubmit = () => {
    //  console.log(this.state.inputItem, "add checkitem in list")
    //  api call to add checkitem in checklist
    CreateACheckIteminCheckList(this.props.checkList.id, this.state.inputItem)
    .then((res) => this.setState({AllCheckItems: [...this.state.AllCheckItems, res.data]}))
    .then((res) => this.setState({addCheckItem: !this.state.addCheckItem}))
    .then((res) => this.setState({inputItem: ""}))
  }

  handleClose = () => {
      this.setState({addCheckItem: !this.addCheckItem })
  }

  handleCheckBox = (e, cardId, checkItemId) => {
    // console.log(e.target.checked, "clicking value of checkbox");
    // console.log(cardId, "on checkbox click")
    // console.log(checkItemId, "on checkbox click");
    UpdateCheckItemsOnCard(e.target.checked, cardId, checkItemId)
    .then((res) => this.setState({AllCheckItems: this.state.AllCheckItems.map((checkitem) =>{
        if(checkitem.id === checkItemId){
            checkitem.state = res.data.state
        }

        return checkitem;
    } )}))

  }



  handleRemoveCheckItem = (checkListId, checkItemId) => {
        //  console.log(checkListId, checkItemId, "for deletion purpose");
        DeleteACheckItemFromCheckList(checkListId, checkItemId)
        .then((res) => this.setState({AllCheckItems: this.state.AllCheckItems.filter((checkitem) => checkitem.id !== checkItemId)}))
  }
  render() {
    //  console.log(this.props.CardDetails);
    console.log(this.props.EachCardDetails.id, "each card id from allcheckitemsinchecklist");
     console.log(this.props.checkList.id, "allchecklistitems");
    return (
        <div>
            {this.state.AllCheckItems.map((checkitem) => {
                return (
                    <div className="all-checkitems">
                    <div className="checkitems-container">
                      <input className="checkitem-checkbox" type="checkbox" onChange={(e) => this.handleCheckBox(e,this.props.EachCardDetails.id,checkitem.id)} checked={checkitem.state==='complete'?true:false}></input>
                      <span className="checkitem-name">{checkitem.name}</span>
                    </div>
                    <div onClick={() => this.handleRemoveCheckItem(this.props.checkList.id, checkitem.id)}><FaTrash className="fa-trash"/></div>
                  </div>
                )
            })}
            
            <div className="addcheckitem-container">
            {this.state.addCheckItem?(
                <button className="btn-primary" onClick={this.handleStateToggle}>Add a item</button>
            ):( <div>
                <input  className="input-check-item" type="text" onChange={this.handleChange} value={this.state.inputItem}/>
                <button  onClick={this.handleSubmit} className="addbtn-checkitem btn-primary">Add</button>
                <button  onClick={this.handleClose}className="btn-danger">Cancel</button>
                 </div>   
            )}
            </div>


        </div>
      
      
    );
  }
}

export default AllCheckItemsinCheckList;
