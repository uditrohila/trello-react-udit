import { ImCross } from "react-icons/im";
import axios from "axios";
import { IoClose } from "react-icons/io5";
import React, { Component } from "react";
import { Modal , Button } from "react-bootstrap";
import Cards from "./Cards";
import { createList } from "./fetchapi";
import { deleteList } from "./fetchapi";
class EachBoardPage extends Component {
  state = {
    lists: [],
    listAddButton: true,
    inputtitleList: "",
    addCardToggle : false
  };
  componentDidMount = () => {
    return axios.get(
      `https://api.trello.com/1/boards/${this.props.match.params.boardId}/lists?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
      .then((res) => this.setState({lists: res.data}))

      // get all cards of list get
    //    axios.get(`https://api.trello.com/1/lists/${id}/cards?key=60f1ecd1d4fe73625acae4db1ce419f8&token=98303c7a34d06af7e3cc8d36aa4116268a04833ed41eaf9ea4b8b560a28c4b0d`)
  };



addButtonToggle = () => {
    this.setState({listAddButton: !this.state.listAddButton})
    this.setState({modalShow: !this.state.modalShow})
}


handleAddListToggle = () => {
    this.setState({modalShow: !this.state.modalShow})
    this.setState({listAddButton: !this.state.listAddButton})
}

handleInput = (e) => {
   this.setState({inputtitleList: e.target.value})
}

handleSumbit = (id) => {
    this.setState({modalShow: !this.state.modalShow});
    this.setState({listAddButton: !this.state.listAddButton})
    // send the title to post api craete a new list
     createList(this.state.inputtitleList, id)
     .then((res) => {
         this.setState({
             lists:[...this.state.lists, res.data],
             inputtitleList: "",
            })
         
     } )
    // console.log(id);
    // console.log(this.state.inputtitleList);
}

handleDeleteList = (id) => {
    // console.log(id, "each board page");
    deleteList(id)
    .then((res) => {
        this.setState({lists: this.state.lists.filter((list) => list.id !== id)})
    })

}

  render() {
    // console.log(this.state.lists, "hello");
    // console.log(this.props, "each page");
    return (
      <div className="list-wrapper d-flex align-items-start">
          {this.state.lists.map((list) => {
              return (
                <div className="outer-list-container">
                <div className="list-container">
                <div className="list-items d-flex justify-content-between">
                  <div className="list-name">{list.name}</div>
                  <span className="remove-list-btn" onClick={() =>  this.handleDeleteList(list.id)}><IoClose/></span>
                </div>
                {<Cards listDetail={list}/>}
                </div>
                </div>

              )
          })}
         {/* <button onClick={this.handleAddList} class="intial-btn">+ Add another list</button> */}
         {this.state.listAddButton?(
                       <div>
                      <button onClick={this.addButtonToggle} className="add-list-btn">+ Add a list</button>
                      </div>
                  ):(
                       <div className="list-create">
                           <input className="list-create-input" type="text" onChange={this.handleInput} value={this.state.inputtitleList} placeholder="Enter list title"></input>
                           <div className="list-create-add-list d-flex"><Button onClick = {() => this.handleSumbit(this.props.match.params.boardId)} className="list-create-btn">Add list</Button> <span onClick={this.handleAddListToggle} className="remove-addlist"><IoClose/></span></div>
                          
                       </div>
                  )}

        </div>

       

     
    );
  }
}

export default EachBoardPage;
